import React, { useState } from "react";
import { ResponsiveTreeMap } from "@nivo/treemap";
const _data = {
  name: "Total",
  color: "hsl(300, 70%, 50%)",
  caca: "orange",
  children: [
    {
      name: "Stocks",
      color: "hsl(142, 70%, 50%)",
      children: [
        {
          name: "AMZN",
          color: "hsl(200, 70%, 50%)",
          loc: 23134.89,
        },
        {
          name: "APPL",
          color: "hsl(200, 70%, 50%)",
          loc: 21223.89,
        },
        {
          name: "GOOGL",
          color: "hsl(200, 70%, 50%)",
          loc: 10022.89,
        },
      ],
    },

    {
      name: "Funds & ETFS",
      color: "hsl(142, 70%, 50%)",
      children: [
        {
          name: "Funds",
          children: [
            {
              name: "FIXED",
              color: "blue",
              children: [
                {
                  name: "JPM GLOBAL BOND OPPORTUNITIES  (USD) INC (M)",
                  color: "orange",
                  loc: 117476.89,
                },
                {
                  name: "AB FCP I - GLOBAL HIGH YIELD PORTFOLIO 'I' (USD) INC",
                  color: "hsl(285, 70%, 50%)",
                  loc: 84535.89,
                },
                {
                  name: "AMUNDI AZIONARIO EUROPA 'A' (EUR) ACC",
                  color: "orange",
                  loc: 117476.89,
                },
              ],
            },
            {
              name: "EQUITIES",
              color: "hsl(285, 70%, 50%)",
              children: [
                {
                  name: "PIMCO INCOME E (USD) INC",
                  color: "hsl(285, 70%, 50%)",
                  loc: 65678.89,
                },
                {
                  name: "SCHRODER ISF GLOBAL HIGH YIELD A1 (USD) INC MF",
                  color: "hsl(285, 70%, 50%)",
                  loc: 12312.89,
                },
              ],
            },
          ],
        },
        {
          name: "ETFS",
          color: "green",
          children: [
            {
              name: "ETF1",
              color: "hsl(200, 70%, 50%)",
              loc: 43243.89,
            },
            {
              name: "ETF2",
              color: "hsl(200, 70%, 50%)",
              loc: 8034.89,
            },
            {
              name: "ETF3",
              color: "hsl(200, 70%, 50%)",
              loc: 1234.89,
            },
          ],
        },
      ],
    },
    {
      name: "Cash & Cash Equivalents",
      color: "hsl(285, 70%, 50%)",
      children: [
        {
          name: "Cash",
          color: "hsl(285, 70%, 50%)",
          loc: 44346.22,
        },
        {
          name: "Cash Equivalents",
          color: "hsl(285, 70%, 50%)",
          loc: 22346.22,
        },
      ],
    },
  ],
};

const findNode = (id, data = _data) => {
  if (id === data.name) return data;

  if (!data.children) return false;

  const found = data.children.find((node) => node.name === id);

  if (found) return found;
  return data.children.map((node) => findNode(id, node)).find((el) => el);
};

const MyResponsiveTreeMap = () => {
  const [data, setData] = useState(_data);

  return (
    <ResponsiveTreeMap
      data={data}
      identity="name"
      value="loc"
      valueFormat=" >-.3s"
      margin={{
        top: 10,
        right: 10,
        bottom: 10,
        left: 10,
      }}
      label={function (e) {
        return `${e.id.substring(0, 10)}...` + ` (${e.formattedValue})`;
      }}
      labelSkipSize={60}
      labelTextColor={{
        from: "color",
        modifiers: [["darker", "3"]],
      }}
      orientLabel={false}
      parentLabelSize={40}
      parentLabelTextColor={{
        from: "color",
        modifiers: [["darker", 2]],
      }}
      nodeOpacity={1}
      borderWidth={0.1}
      borderColor="black"
      onClick={(node, event) => {
        if (node.treeDepth === 0) {
          const a = _data.children.find(({ name }) => name === node.id);

          setData(_data);
          return;
        }
        const a = findNode(node.id);
        setData(a);
      }}
    />
  );
};
function App() {
  return (
    <div
      style={{
       
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <h1>Demo Treemap</h1>
      <div  style={{
        width: "80vw",
        height: "80vh",
       
      }}>
      <MyResponsiveTreeMap></MyResponsiveTreeMap>
      </div>
    </div>
  );
}

export default App;
